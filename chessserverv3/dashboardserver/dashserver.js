var app = require('express')();
var http = require('http').createServer(app);


var io = require('socket.io')(http);
const port = 3000;

let isconnected = true;

var redis = require("redis");

client = redis.createClient();

client.on("error", function (err) {
    console.log("Error " + err);
});

client.subscribe('connect-channel');

client.on('message', (channel,message) => {

    if(channel === 'connect-channel'){
       
        try{
            message = JSON.parse(message);
        }catch(err){
            console.error("Not json parseable");
            console.error(message);
    
        }
        console.log(message.event);
        if(message.to === "DashServer" && message.event === "BoardsReady"){
            console.log("HELLO");
           sendToDashSocketReadyBoards(message.channelId);
            
        }


    }
  

})

io.on('connection', function(socket){
    console.log('a user connected');

    
    //io.emit('channelMessage',{"hello" : "hello"});
    //sendToDashSocketReadyBoards(12);
    
    socket.on('disconnect', function(){
        console.log('user disconnected');
        
    });
});

  


function sendToDashSocketReadyBoards(channelId){

    if(isconnected === true){

        var sendtochanel =  {"channelId":channelId, "to":"Dashborad", "event":"BoardsReady"};
        sendtochanel = JSON.stringify(sendtochanel);
        io.sockets.emit('channelMessage',sendtochanel);
        console.log("emitted");
    }

}


http.listen(port, () => console.log(`Dashboard Server listening on port ${port}!`));