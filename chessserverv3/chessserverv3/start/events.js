'use strict'

const Redis = use('Redis')

const Match = use('App/Models/Match')
const User = use('App/Models/User')

Redis.subscribe('connect-channel', async (message) => {

    
   
    try{
        message = JSON.parse(message);
    }catch(err){
        console.error("Not json parseable");
        console.error(JSON.stringify(message));

    }

    console.log(typeof message);

    if(message.to == "ChessServer" && message.event == "BoardsMake"){
        
        let channelId = message.channelId;

        const users = await User
            .query()
            .where('chanel_id', channelId)
            .fetch();
        const usersJSON = users.toJSON();
    
        var members = Object.keys(usersJSON).length;
    
        var halfMembers = members/2;
    
        var whitePlayersIds = new Array();
        var blackPlayersIds = new Array();
    
        var matchMaking = true;
        var positions = new Array();
    
        var round = 0;
        while(matchMaking){
    
            if(round === 0){
    
                var x = Math.floor((Math.random() * (members)) + 1);
                if(!positions.includes(x)){
                    whitePlayersIds.push(usersJSON[x-1].id);
                    positions.push(x)
                }
    
                if(halfMembers === whitePlayersIds.length){
                    round = 1;
                }
    
            }else{
    
                var x = Math.floor((Math.random() * (members)) + 1);
                if(!positions.includes(x)){
                    blackPlayersIds.push(usersJSON[x-1].id);
                    positions.push(x)
                }
    
                if(halfMembers === blackPlayersIds.length){
                    matchMaking = false;
                }
    
            }
    
        }
    
        console.log("White Users");
        console.log(whitePlayersIds);
        console.log("Black Users:");
        console.log(blackPlayersIds);
    
        for(var i = 0; i < halfMembers; i++){
    
            const board = new Match()
    
            board.user_1 = whitePlayersIds[i];
            board.user_2 = blackPlayersIds[i];
            board.closed = false;
    
            await board.save()
    
        }

        var sendback = {"channelId":channelId, "to":"DashServer", "event":"BoardsReady"};
        sendback = JSON.stringify(sendback);
        Redis.publish('connect-channel', sendback )
        

    }

        
})



