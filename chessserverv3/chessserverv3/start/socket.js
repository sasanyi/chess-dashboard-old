'use strict'

/*
|--------------------------------------------------------------------------
| Websocket
|--------------------------------------------------------------------------
|
| This file is used to register websocket channels and start the Ws server.
| Learn more about same in the official documentation.
| https://adonisjs.com/docs/websocket
|
| For middleware, do check `wsKernel.js` file.
|
*/

const Match = use('App/Models/Match')
const User = use('App/Models/User')
const Channel = use('App/Models/Channel')
const Chess = use('chess.js').Chess;

const Ws = use('Ws')
let boards = {};



Ws.channel('chat', ({ socket }) => {
  console.log('user joined with %s socket id', socket.id)
})




Ws.channel('board:*', ({ socket }) => {

  let act_topic = socket.topic;
  console.log(act_topic)
  console.log(boards[act_topic]);
  if(boards[act_topic] === undefined ){

    boards[act_topic] = {};
    boards[act_topic]['ids'] = new Array();
    boards[act_topic]['ids'].push(socket.id);
    boards[act_topic]['game'] = new Chess();
  
  }else{
    console.log("boards");

    boards[act_topic]['ids'].push(socket.id);

  }

  console.log(boards[act_topic]['ids']);


  //lose event

  socket.on('match-lost', async (message) => {   
    console.log("END")
    console.log(act_topic)
    console.log(act_topic.split(":"))
    let id = act_topic.split(":")[1]
    console.log(id)
    
    let board = await Match.find(id)
    console.log(board)
    board.closed = 1
    board.save()

    board = board.toJSON()
    console.log(board)

    let user_white = board.user_1
    let user_black = board.user_2

    if(message.loser == "w"){
      let user = await User.find(user_white)
      console.log(user)
      let channel_id = user.chanel_id
      console.log(channel_id)
      let channel = await Channel.findBy('chanel_id', channel_id)
      channel = channel.toJSON()
      let price = channel.chanel_price
      let slot = channel.chanel_slot
      let ukredit = user.kredit
      if(ukredit == null){
        ukredit = 0
      }
      console.log(ukredit, price)
      user.kredit = ukredit - price
      user.chanel_id = null
      user.in_game = 0
      user.save()

      let players = await User.findBy("chanel_id", channel_id)
      
      if(players.length > 1){
        let find = false
        players.forEach(element => {
          element = eleemnt.toJSON()
          if(element.in_game == 0){

            let newMatch = new Match()
            newMatch.user_1 = element.id
            newMatch.user_2 = user_black
            newMatch.save()

            find = true

          }
        });

        if(find == true){
          //emit restart

          socket.emitTo('restart', "", boards[act_topic]['ids'])
          delete boards[act_topic]

          console.log("Restart")
        }else{
          //emit wait

          socket.emitTo('wait', "", boards[act_topic]['ids'])
          delete boards[act_topic]

          let win = await User.find(user_black)
          win.in_game = 0
          win.save()
        }
      }else{
        let winner = await User.find(user_black)
        let ukredit = winner.kredit
        if(ukredit == null){
          ukredit = 0
        }
        winner.in_game = 0
        winner.kredit = ukredit + (slot * price)
        winner.chanel_id = null
        console.log(ukredit, slot ,price)

        winner.save()

        console.log("Won the full match ")

      }



    }else{
      let user = await User.find(user_black)
      
      let channel_id = user.chanel_id
      let channel = await Channel.findBy('chanel_id', channel_id)
      channel = channel.toJSON()
      let price = channel.chanel_price
      let slot = channel.chanel_slot
      let ukredit = user.kredit
      if(ukredit == null){
        ukredit = 0
      }
      console.log(ukredit, price)
      user.kredit = ukredit - price
      user.chanel_id = null
      user.in_game = 0
      user.save()

      let players = await User.findBy("chanel_id", channel_id)
      
      if(players.length > 1){
        let find = false
        players.forEach(element => {
          element = eleemnt.toJSON()
          if(element.in_game == 0){

            let newMatch = new Match()
            newMatch.user_1 = element.id
            newMatch.user_2 = user_white
            newMatch.save()

            find = true

          }
        });

        if(find == true){
          console.log("Restart")

          socket.emitTo('restart', "", boards[act_topic]['ids'])
          delete boards[act_topic]

        }else{
        
          socket.emitTo('wait', "", boards[act_topic]['ids'])
          delete boards[act_topic]

          let win = await User.find(user_white)
          win.in_game = 0
          win.save()
        }
      }else{
        let winner = await User.find(user_black)
        let ukredit = winner.kredit
        console.log(ukredit, slot, price)
        if(ukredit == null){
          ukredit = 0
        }
        winner.in_game = 0
        winner.kredit = ukredit + (slot * price)
        winner.chanel_id = null
        winner.save()
        
        console.log("Won the full match ")

      }

    }

    socket.emitTo('match-end', message, boards[act_topic]['ids'])
  });
  


  //start event

 

  //move event

  socket.on('move', (message) => {
    console.log("MOVE", message)
    console.log(boards[act_topic]['ids'])


    var master = boards[act_topic]['game'];
    //move check for +/- time
    console.log(master.ascii());

    master.move(message)
    
    console.log(master.ascii());
    
    var round = message["color"]
   
      
    master.setTurn(round);

    console.log(master.turn());
    

    var available_moves = master.moves({square: message['to'], verbose:true});

    console.log(available_moves);

    //promotion
    if(message['flags'].includes('p'))
    {
      console.log("promotion");

      socket.emitTo('add-time', {color: master.turn(), time: 60},boards[act_topic]['ids'])

    }

    //double attact
    let available_captures = 0;
    let double_chess = false;
    available_moves.forEach(function(move){

      if(move['flags'].includes('c') && move['from'] && (move['piece'] == 'n' || move['piece'] == 'q' || move['piece'] == 'k' || move['piece'] == 'p') && move['captured'] == 'k'){
        double_chess = true;
        
      }
      
      if(move['flags'].includes('c') && move['from'] && (move['piece'] == 'n' || move['piece'] == 'q' || move['piece'] == 'k' || move['piece'] == 'p') && move['captured'] != 'p'){
        ++available_captures;
        
      }

    });

    if(available_captures >= 2 && double_chess == false){
      console.log("double attack")
      socket.emitTo('add-time', {color: master.turn(), time: 20},boards[act_topic]['ids'])

    }

    //double chess

    if(available_captures >= 2 && double_chess == true){
      console.log("double chess")
      socket.emitTo('add-time', {color: master.turn(), time: 30},boards[act_topic]['ids'])

    }

    if(master.turn() == 'w')
    {
      master.setTurn('b')
    }
    else
    {

      master.setTurn('w')

    }
    socket.emitTo('move',message,boards[act_topic]['ids'])

  });
  
  


  //socket.emitTo('greeting', 'hello2', boards[act_topic]['ids']);
  //socket.on('event', function)

  
})

let availableMoves = (symGame, from) => {
  let gamePGN = symGame.pgn()

    let tokens = symGame.fen().split(' ')
    tokens[1] = tokens[1] === 'w' ? 'b' : 'w'
    symGame.load(tokens.join(' '))

    let moves = symGame.moves({square: from, verbose: true})

    tokens = symGame.fen().split(' ')
    tokens[1] = tokens[1] === 'w' ? 'b' : 'w'
    symGame.load_pgn(gamePGN)

    return moves
}
