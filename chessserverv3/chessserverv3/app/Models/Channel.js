'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Channel extends Model {

    static get table () {
        return 'chanels'
      }

}

module.exports = Channel
