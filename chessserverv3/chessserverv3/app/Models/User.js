'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {

    static get table () {
        return 'users'
    }

    static get hidden () {
      return ['email','password', 'idnum', 'zip', 'city', 'address1', 'address2', 'phone', 'permissions', 'last_login', 'first_login', 'avatar', 'api_token', 'created_at', 'updated_at']
    }


}

module.exports = User
