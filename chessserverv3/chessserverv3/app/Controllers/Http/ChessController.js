'use strict'

const Database = use('Database')
const Match = use('App/Models/Match')
const User = use('App/Models/User')


class ChessController {

    async index({params, view ,request, response}){

        console.log(params.userId)

        let user = await User
        .query()
        .where('securecode', params.userId)
        .fetch();
       
       user = user.toJSON();

       let board = await Match
       .query()
       .where(function(){
        this
        .where('user_1', user[0].id)
        .orWhere('user_2',  user[0].id)

       })
       .fetch();
      
      board = board.toJSON();
       let color = "black"
      if(board[0].user_1 == user[0].id){
        console.log("white")
        color = "white"
      }
       
      if(board[0].closed){
        response.redirect('http://chessv3/dashboard')
      }
       
      console.log(board)
       
        return view.render('chess', {'user': user[0], 'board' : board[0].id, 'color':color});

    }

}

module.exports = ChessController
