'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MatchesSchema extends Schema {
  up () {
    this.createIfNotExists('matches', (table) => {
      table.increments()
      table.timestamps()
      table.integer("user_1").unsigned().references("id").inTable("users").notNullable()
      table.integer("user_2").unsigned().references("id").inTable("users").notNullable()
      table.boolean("closed").defaultTo(false).notNullable()
    })
  }

  down () {
    this.drop('matches')
  }
}

module.exports = MatchesSchema
