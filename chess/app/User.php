<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Hootlex\Friendships\Traits\Friendable;
use Sentinel;

class User extends \Cartalyst\Sentinel\Users\EloquentUser
{
    use Notifiable;
    use Friendable;

    public static function findByEmail($email){
        $credentials = [
            'email' => $email,
        ];

        return Sentinel::findByCredentials($credentials);
    }

}
