var express = require('express');
var router = express.Router();


var sqlite3 = require('sqlite3').verbose();



function logTime(request, response, next){


    console.log("Time: ", Date.now());
    next();

}

function logMethod(request, response, next){


    console.log("Method: ", request.method);
    next();

}

function logUrl(request, response, next){


    console.log("Method: ", request.method);
    next();

}

var logStuff = [logTime,logMethod,logUrl];

router.use(logStuff);

router.get('/', function (request, response, next) {
    response.send("Lobbys");
});

router.get('/:lobbyId/:userId',function (request, response, next) {


    var lobbyId = request.params.lobbyId;
    var userId = request.params.userId;

    response.send(lobbyId + "" + userId);
});

router.post('/', function(request, response, next){

    let db = new sqlite3.Database('chessapp.db', sqlite3.OPEN_READWRITE ,(err)=>{
        if(err){

            console.error(err.message);

        }

    });

    var lobbytype = request.body.lobbyType;
    var slots = request.body.slots;

    console.log(request.body);


        var d = db.run("Insert INTO lobby (slot, type_id) VALUES (?, ?)", [slots ,lobbytype], function(err){

            if(err){
                console.log(err.message);
            }

        });


    db.close();


    response.sendStatus(200);

});

module.exports = router;